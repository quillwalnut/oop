#include "stdafx.h"
#include "container_atd.h"

using namespace std;

namespace simple_shapes
{
	void container::In(ifstream &in)
	{
		while (!in.eof())
		{
			if (len < max_len)
			{
				if ((cont[len] = shape::In(in)) != 0)
						len++;
			}
			else
				break;
		}
	}
}