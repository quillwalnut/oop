#include "stdafx.h"
#include "triangle_atd.h"
#include "shape_atd.h"

using namespace std;

namespace simple_shapes
{
	void triangle::InData(ifstream &in)
	{
		in >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
	}
}