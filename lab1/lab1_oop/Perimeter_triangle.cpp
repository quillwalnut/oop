#include "stdafx.h"
#include "triangle_atd.h"

namespace simple_shapes
{
	float triangle::Perimeter()
	{
		float a = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
		float b = sqrt(pow(x2 - x3, 2) + pow(y2 - y3, 2));
		float c = sqrt(pow(x1 - x3, 2) + pow(y1 - y3, 2));
		return a + b + c;
	}
}