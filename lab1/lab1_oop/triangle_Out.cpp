#include "stdafx.h"
#include "triangle_atd.h"

using namespace std;

namespace simple_shapes
{
	void triangle::Out(ofstream &out)
	{
		out << "It is Triangle: x1 = " << x1 << ", y1 = "
			<< y1 << ", x2 = " << x2 << ", y2 = " << y2 << ", x3 = " 
			<< x3 << ", y3 = " << y3 << ", color = " << printColor(color) << endl;
	}
}