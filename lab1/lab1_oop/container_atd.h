#ifndef __container_atd__
#define __container_atd__

#include "shape_atd.h"

namespace simple_shapes
{
	class container
	{
		enum 
		{
			max_len = 100 // максимальная длина
		};
		int len; // Текущая длина
		shape *cont[max_len];
	public:
		void In(ifstream &in);
		void Out(ofstream &out);
		void Sort(); // сортировка контейнера
		void Clear(); 
		void Perimeter(ofstream &ofst);
		void OutCircle(ofstream &out);
		container(); // инициализация контейнера
		~container()
		{ 
			Clear(); 
		}
	};
}
#endif // !__container_atd__
