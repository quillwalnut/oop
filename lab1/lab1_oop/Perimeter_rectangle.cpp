#include "stdafx.h"
#include "rectangle_atd.h"

namespace simple_shapes
{
	float rectangle::Perimeter()
	{
		float p = 2.0*(sqrt(pow(x1 - x2, 2)) + sqrt(pow(y1 - y2, 2)));
		return p;
	}
}