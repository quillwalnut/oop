#ifndef __shape_atd__
#define __shape_atd__

#include <fstream>
#include <string>
using namespace std;

namespace simple_shapes
{
	class shape
	{

	public:
		static shape* In(ifstream &in);
		virtual void InData(ifstream &in) = 0;
		virtual void Out(ofstream &out) = 0;
		virtual float Perimeter() = 0;
		bool Compare(shape &other);
		virtual void OutCircle(ofstream &out);
	protected:
		shape() {};
		enum colorEnum { RED, ORANGE, YELLOW, GREEN, BLUE, DARKBLUE, VIOLET };
		string printColor(colorEnum color);
		colorEnum color;
		int density;
	};
}
#endif // ! __shape_atd__