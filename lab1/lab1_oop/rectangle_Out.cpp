#include "stdafx.h"
#include "rectangle_atd.h"

using namespace std;

namespace simple_shapes
{
	void rectangle::Out(ofstream &out)
	{
		out << "It is Rectangle: x1 = " << x1 << ", y1 = "
			<< y1 << ", x2 = " << x2 << ", y2 = " << y2 << ", color = " 
			<< printColor(color) << ", density = " << density << endl;		
	}
}