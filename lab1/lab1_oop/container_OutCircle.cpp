#include "stdafx.h"
#include "container_atd.h"

using namespace std;

namespace simple_shapes
{
	void container::OutCircle(ofstream &out) 
	{
		out << "Only circles." << endl;
		for (int i = 0; i < len; i++) 
		{
			out << i << ": ";
			cont[i]->OutCircle(out);
		}
	}
}