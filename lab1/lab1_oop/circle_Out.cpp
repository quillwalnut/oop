#include "stdafx.h"
#include "circle_atd.h"

using namespace std;

namespace simple_shapes
{
	void circle::Out(ofstream &out)
	{
		out << "It is Circle: x = " << x << ", y = "
			<< y << ", r = " << r << ", color = " << printColor(color) << ", density = " << density << endl;
	}
}