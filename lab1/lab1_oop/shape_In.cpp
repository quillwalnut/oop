#include "stdafx.h"
#include "shape_atd.h"
#include "rectangle_atd.h"
#include "circle_atd.h"
#include "triangle_atd.h"

using namespace std;

namespace simple_shapes
{
	shape* shape::In(ifstream &in)
	{
		shape *sp;
		int k;
		in >> k;
		switch (k)
		{
		case 1:
			sp = new circle;
			break;
		case 2:
			sp = new rectangle;
			break;
		case 3:
			sp = new triangle;
			break;
		default:
			return 0;
		}
		sp->InData(in);

		int currColor;
		in >> currColor;
		in >> sp->density;
		switch (currColor)
		{
		case 1:
			sp->color = RED;
			break;
		case 2:
			sp->color = ORANGE;
			break;
		case 3:
			sp->color = YELLOW;
			break;
		case 4:
			sp->color = GREEN;
			break;
		case 5:
			sp->color = BLUE;
			break;
		case 6:
			sp->color = DARKBLUE;
			break;
		case 7:
			sp->color = VIOLET;
			break;
		}
		return sp;
	}
}