#ifndef __circle_atd__
#define __circle_atd__

#include "shape_atd.h"

namespace simple_shapes
{
	class circle : public shape
	{
		int x, y; // ���������� ������ ����������
		int r; // ������
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		float Perimeter();
		void OutCircle(ofstream &out);
		circle() {} // �������� ��� ��������������
	};
}
#endif // ! __circle_atd__