#ifndef __rectangle_atd__
#define __rectangle_atd__

#include "shape_atd.h"

namespace simple_shapes
{
	class rectangle : public shape
	{
		int x1, y1; // ���������� ������ �������� ����
		int x2, y2; // ���������� ������� ������� ����
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		float Perimeter();
		rectangle() {} // �������� ��� ��������������
	};
}
#endif // ! __rectangle_atd__