#ifndef __triangle_atd__
#define __triangle_atd__

#include "shape_atd.h"

namespace simple_shapes
{
	class triangle : public shape
	{
		int x1, y1, x2, y2, x3, y3; // ���������� ���� ������
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		float Perimeter();
		triangle() {} // �������� ��� ��������������
	};
}
#endif // ! __triangle_atd__
