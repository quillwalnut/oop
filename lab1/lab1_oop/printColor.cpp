#include "stdafx.h"
#include "shape_atd.h"

namespace simple_shapes
{
	string shape::printColor(colorEnum color1)
	{
		switch (color)
		{
		case RED:
			return "RED";
		case ORANGE:
			return "ORANGE";
		case YELLOW:
			return "YELLOW";
		case GREEN:
			return "GREEN";
		case BLUE:
			return "BLUE";
		case DARKBLUE:
			return "DARKBLUE";
		case VIOLET:
			return "VIOLET";
		}
		return "error";
	}
}