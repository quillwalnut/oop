#include "stdafx.h"
#include "container_atd.h"

using namespace std;

namespace simple_shapes
{
	void container::Out(ofstream &out)
	{
		out << "Container contents " << len
			<< " elements." << endl;
		Sort();
		for (int i = 0; i < len; i++)
		{
			out << i << ": ";
			cont[i]->Out(out);
			out << "perimeter = " << cont[i]->Perimeter() << endl;
		}
	}
}