#include "stdafx.h"
#include "circle_atd.h"

namespace simple_shapes
{
	float circle::Perimeter()
	{
		float p = r * 2.0 * 3.14;
		return p;
	}
}