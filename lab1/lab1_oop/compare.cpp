#include "stdafx.h"
#include "shape_atd.h"

using namespace std;

namespace simple_shapes
{
	bool shape::Compare(shape &other)
	{
		return Perimeter() < other.Perimeter();
	}}